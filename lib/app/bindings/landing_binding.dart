import 'package:get/get.dart';

import '../controller/landing_controller.dart';
import 'setting_binding.dart';

class LandingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LandingController>(
      () => LandingController(),
    );

    SettingBinding().dependencies();
  }
}
