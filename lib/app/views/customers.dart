import 'dart:developer';

import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:water_counter/app/controller/main_controller.dart';
import 'package:water_counter/app/core/db/sqlite_service.dart';
import 'package:water_counter/app/views/AppBar.dart';
import 'package:water_counter/app/views/Toast.dart';
import 'package:water_counter/app/views/customerElement.dart';

class Customers extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainController>(
      initState: (state) {
        Future.delayed(Duration(microseconds: 1)).then((value) async {
          bool check =
              await state.controller!.handleLocationPermission(context);
          if (!check) {
            Get.back();
          }
        });
      },
      builder: (controller) {
        return Scaffold(
          body: SafeArea(
            child: Column(
              children: [
                InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () async {
                    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
                    log(position.altitude.toString());
                    log(position.longitude.toString());
                  },
                  child: appBar("لیست مشترکین", margin: 0),
                ),
                InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () async {
                    controller.selectingsort.toggle();
                    controller.update();
                  },
                  child: Container(
                    height: 55,
                    width: double.infinity,
                    color: Colors.grey[200],
                    padding: EdgeInsets.symmetric(horizontal: 18),
                    child: Row(
                      children: [
                        Icon(
                          Icons.sort,
                        ),
                        SizedBox(width: 18),
                        // Icon(),
                        Spacer(),
                        (controller.sort.isNotEmpty ||
                                controller.isSearch.isTrue)
                            ? InkWell(
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () async {
                                  controller.sort.value = "";
                                  controller.isSearch.value = false;
                                  controller.search.value = "";
                                  controller.searchcontroller.text = "";
                                  controller.getCustomersFromDB();
                                  controller.update();
                                },
                                child: Icon(
                                  Icons.close,
                                  size: 16,
                                ),
                              )
                            : Container(width: 0),
                        SizedBox(width: 9),
                        Text(
                          controller.isSearch.isTrue
                              ? controller.search.value
                              : controller.sort.isEmpty
                                  ? "مرتب سازی پیشفرض"
                                  : controller.sort.value,
                        ),
                      ],
                    ),
                  ),
                ),
                AnimatedContainer(
                  duration: Duration(milliseconds: 250),
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(10))),
                  padding: EdgeInsets.symmetric(horizontal: 18),
                  height: controller.selectingsort.isTrue
                      ? (45 * (controller.sortingList.length)).toDouble() + 53
                      : 0,
                  width: double.infinity,
                  child: ListView.builder(
                    padding: EdgeInsets.zero,
                    physics: BouncingScrollPhysics(),
                    itemCount: controller.sortingList.length + 1,
                    itemBuilder: (context, index) {
                      if (index == 0) {
                        return Container(
                          height: 53,
                          width: double.infinity,
                          padding: EdgeInsets.only(left: 24),
                          decoration: BoxDecoration(
                            color: Color(0xffEDEDED),
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Row(
                            textDirection: TextDirection.rtl,
                            children: [
                              InkWell(
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () {
                                  controller.isSearch.value = true;
                                  controller.selectingsort.value = false;
                                  controller.getCustomersFromDB();
                                },
                                child: Container(
                                  height: 53,
                                  width: 53,
                                  margin: EdgeInsets.symmetric(vertical: 6),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.blue,
                                  ),
                                  child: Icon(
                                    Icons.search,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              SizedBox(width: 8),
                              Expanded(
                                child: TextFormField(
                                  controller: controller.searchcontroller,
                                  keyboardType: TextInputType.number,
                                  // focusNode: node,
                                  // onTap: onTap,
                                  onChanged: (e) {
                                    // controller.nameValidator(e);
                                    log("text: $e");
                                    // update();
                                  },
                                  onFieldSubmitted: (e) {
                                    controller.isSearch.value = true;
                                    controller.selectingsort.value = false;
                                    controller.getCustomersFromDB();
                                  },
                                  // obscureText: !showContent!,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w300,
                                    color: Color(0xff2F2C2C),
                                  ),
                                  maxLines: 1,
                                  minLines: 1,
                                  textAlign: TextAlign.right,
                                  textDirection: TextDirection.rtl,
                                  decoration: InputDecoration(
                                    hintTextDirection: TextDirection.rtl,
                                    alignLabelWithHint: true,
                                    hintStyle: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w300,
                                      color: Color(0xff898D92),
                                    ),
                                    hintMaxLines: 1,
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    errorBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide(
                                        color: Color(0xffFF3333),
                                        width: 1,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () async {
                            controller.isSearch.value = false;
                            controller.sort.value =
                                controller.sortingList[index - 1].toString();
                            controller.getCustomersFromDB();
                            controller.selectingsort.toggle();
                            controller.update();
                          },
                          child: Container(
                            height: 45,
                            alignment: Alignment.centerRight,
                            child: Text(
                              controller.sortingList[index - 1].toString(),
                            ),
                          ),
                        );
                      }
                    },
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    padding: EdgeInsets.zero,
                    itemCount: controller.customers.length,
                    itemBuilder: (context, index) {
                      return cart(
                          customer: controller.customers[index], index: index);
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
