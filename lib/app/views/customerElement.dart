import 'dart:developer';

import 'package:flutter_share/flutter_share.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:shamsi_date/shamsi_date.dart';
import 'package:water_counter/app/controller/main_controller.dart';
import 'package:water_counter/app/core/db/sqlite_service.dart';
import 'package:water_counter/app/models/customer.dart';
import 'package:water_counter/app/views/AppBar.dart';
import 'package:water_counter/app/views/Toast.dart';

Widget cart({required Customer customer, required int index}) {
  // 638050183086482397
  // 592
  MainController maincontroller = Get.find();
  double elementh = 45;
  String showingDate = "";
  DateTime? myDateTime;
  // if ((customer.ondate == null) || (customer.ondate!.isEmpty)) {
  myDateTime = DateTime.parse(customer.lastUpdateNumberOnDate!.value);
  // } else {
  //   myDateTime = DateTime.parse(customer.ondate!.string);
  // }
  // if (condition) {

  // }
  Jalali perDate = new Jalali(1999);
  if (customer.lastUpdateNumberOnDate!.string != "0000-00-00T00:00:00") {
    perDate = myDateTime.toJalali();
  }
  // showingDate = myDateTime.day.toString() +
  //     " : " +
  //     myDateTime.month.toString() +
  //     " : " +
  //     myDateTime.year.toString();
  showingDate = perDate.day.toString() +
      " : " +
      perDate.month.toString() +
      " : " +
      perDate.year.toString();
  String counterDec = "کنتور : ";
  if (customer.waterMeterInstallType!.value == 0) {
    counterDec += "دائمی | ";
  } else {
    counterDec += "موقت  |  ";
  }
  if (customer.waterMeterType!.value == 0) {
    counterDec += "آب شرب | ";
  } else {
    counterDec += "صنعتی  |  ";
  }
  if (customer.waterMeterState!.value == 1) {
    counterDec += "خراب ";
  } else {
    counterDec += "سالم ";
  }
  return InkWell(
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    onTap: (() {
      log(customer.contractCode!.string + " : contractCode");
      log(customer.subscriptionNumber!.string + " : subscriptionNumber");
      log(customer.pelak!.string + " : pelak");
      log(customer.lat!.string);
      log(customer.absence!.string + " : absence");
      log(customer.long!.string);
      log(customer.lastUpdate!.string + " : lastUpdate");
      log(customer.ondate!.value.toString() + " : onDate");
      log(customer.highlightColor!.value.toString() + " : highlightColor");
      log(((customer.lastNumber == null || customer.lastNumber!.value == 0)
          .toString()));
      log(customer.lastNumber!.value.toString() + " : lastNumber");
      log(customer.lastUpdate!.value + " : lastUpdate");
      log(customer.userCompany!.value + " : userCompany");
    }),
    child: Card(
      elevation: 8,
      shadowColor: Colors.black12,
      margin: EdgeInsets.only(right: 18, left: 18, top: 12),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      clipBehavior: Clip.hardEdge,
      color: ((customer.lastUpdate!.value == null) ||
              customer.lastUpdate!.isEmpty ||
              customer.lastUpdate!.value == "0")
          ? Colors.white
          : Colors.green.withOpacity(0.2),
      child: AnimatedContainer(
        duration: Duration(milliseconds: 500),
        height: (customer.companyAddress == null) ||
                (customer.companyAddress!.isEmpty)
            ? 240.5 + 90 + 50
            : 285.5 + 90 + 50,
        width: double.infinity,
        color: customer.absence!.isTrue
            ? Colors.grey
            : (customer.highlightColor == null ||
                    customer.highlightColor!.value == -1)
                ? Colors.transparent
                : Colors.green,
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 10),
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Column(
            children: [
              // name
              Container(
                // color: Colors.red,
                height: elementh,
                child: Row(
                  children: [
                    Text(
                      "نام مشترک: ",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        customer.userCompany!.value,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
              // address
              (customer.companyAddress == null) ||
                      (customer.companyAddress!.isEmpty)
                  ? Container()
                  : Container(
                      // color: Colors.red,
                      height: elementh,
                      alignment: Alignment.centerRight,
                      child: Text(
                        "آدرس: " + customer.companyAddress!.string,
                        textAlign: TextAlign.right,
                        textDirection: TextDirection.rtl,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
              SizedBox(height: 4),
              Container(
                height: 0.5,
                width: double.infinity,
                color: Colors.red,
              ),
              // شماره اشتراک/ شماره پرونده
              Container(
                // color: Colors.blue,
                height: elementh,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        "شماره اشتراک : " + customer.subscriptionNumber!.string,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "کد قرارداد : " + customer.contractCode!.string,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
              InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () {
                  log("hi: " + customer.mobileNumber!.string);
                  FlutterShare.share(
                    title: customer.mobileNumber!.string,
                    text: customer.mobileNumber!.string,
                  );
                },
                child: Container(
                  height: elementh,
                  // color: Colors.red,
                  alignment: Alignment.centerRight,
                  child: Text(
                    customer.mobileNumber != null
                        ? "موبایل : " + customer.mobileNumber!.value
                        : "شماره تلفن ثبت نشده است",
                  ),
                ),
              ),
              Container(
                height: 0.5,
                width: double.infinity,
                color: Colors.red.shade100,
              ),
              // کنتور ها
              Container(
                // color: Colors.green,
                height: elementh,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        "آخرین رقم قبض: " + customer.lastCheckoutNumber!.string,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "شماره کنتور فعلی : " +
                            ((customer.lastNumber == null ||
                                    customer.lastNumber!.value == 0)
                                ? customer.currentNumber!.string
                                : customer.lastNumber!.value.toString()),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
              customer.lastUpdateNumberOnDate!.value == "0000-00-00T00:00:00"
                  ? Container()
                  : Container(
                      height: elementh,
                      // color: Colors.red,
                      alignment: Alignment.centerRight,
                      child: Text(
                        "تاریخ آخرین قرائت : " + showingDate,
                        textDirection: TextDirection.rtl,
                        textAlign: TextAlign.right,
                      ),
                    ),
              Container(
                height: 0.5,
                width: double.infinity,
                color: Colors.red.shade100,
              ),
              // counter deteail
              Container(
                height: elementh,
                // color: Colors.red,
                alignment: Alignment.centerRight,
                child: Text(
                  counterDec,
                ),
              ),
              Container(
                height: 0.5,
                width: double.infinity,
                color: Colors.red.shade100,
              ),
              Container(
                height: elementh,
                child: Row(
                  children: [
                    Checkbox(
                      value: customer.absence!.value,
                      onChanged: (value) async {
                        log(value.toString() + " : 1");
                        if (value == true) {
                          var res = await Get.bottomSheet(
                            Container(
                              height: 260,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(22)),
                              ),
                              child: Column(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.vertical(
                                        top: Radius.circular(22)),
                                    child: appBar(customer.userCompany!.string),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 18, right: 18, left: 18),
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      "آیا مشترک مورد نظر در محل حضور ندارد و به کنتور دسترسی ندارید ؟",
                                      textAlign: TextAlign.right,
                                      textDirection: TextDirection.rtl,
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black87,
                                      ),
                                    ),
                                  ),
                                  Spacer(),
                                  Container(
                                    height: 45,
                                    width: double.infinity,
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 18, vertical: 26),
                                    child: Row(
                                      textDirection: TextDirection.rtl,
                                      children: [
                                        Expanded(
                                          child: InkWell(
                                            onTap: () {
                                              Get.back(result: 200);
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                color: Colors.greenAccent,
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              alignment: Alignment.center,
                                              child: Text("بله"),
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 8),
                                        Expanded(
                                          child: InkWell(
                                            onTap: () {
                                              Get.back(result: 400);
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                color: Color.fromARGB(
                                                    255, 253, 144, 144),
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                              alignment: Alignment.center,
                                              child: Text(
                                                "خیر",
                                                style: TextStyle(),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                          log(res.toString() + " : res");
                          if (res == 200) {
                            log(value!.toString() + " : 2");
                            int tmp = await SqliteService().changeabsenc(
                              id: customer.id!.value.toString(),
                              newAbsence: 1,
                            );
                            log(tmp.toString());
                            maincontroller.getCustomersFromDB();
                            maincontroller.update();
                          }
                        } else {
                          log("mmd");
                          int tmp = await SqliteService().changeabsenc(
                            id: customer.id!.value.toString(),
                            newAbsence: 0,
                          );
                          log(tmp.toString());
                          maincontroller.getCustomersFromDB();
                          maincontroller.update();
                        }
                      },
                    ),
                    Expanded(
                      child: Text(
                        "عدم توانایی خوانش کنتور به دلیل عدم حضور مشترک",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 13),
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              // btns
              Row(
                textDirection: TextDirection.ltr,
                children: [
                  // Expanded(
                  //   child: InkWell(
                  //     splashColor: Colors.transparent,
                  //     highlightColor: Colors.transparent,
                  //     child: Container(
                  //       height: 35,
                  //       padding: EdgeInsets.symmetric(horizontal: 8),
                  //       decoration: BoxDecoration(
                  //         color: (customer.lastUpdateNumberOnDate == null ||
                  //                 customer.lastUpdateNumberOnDate!.isEmpty)
                  //             ? Colors.white
                  //             : Colors.greenAccent,
                  //         borderRadius: BorderRadius.circular(6),
                  //       ),
                  //       alignment: Alignment.center,
                  //       child: Text(
                  //         "ویرایش ترتیب",
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  // SizedBox(width: 6),
                  Expanded(
                    flex: 10,
                    child: InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () async {
                        customer.highlightColor!.value = 0;
                        maincontroller.update();
                        var res = await Get.bottomSheet(
                          Container(
                            height: 250,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(22)),
                            ),
                            child: Column(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(22)),
                                  child: appBar(customer.userCompany!.string),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 18, right: 18, left: 18),
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    customer.waterMeterState!.value == 1
                                        ? "آیا کنتور تعمیر شده است ؟"
                                        : "آیا کنتور خراب شده است ؟",
                                    textAlign: TextAlign.right,
                                    textDirection: TextDirection.rtl,
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black87,
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  height: 45,
                                  width: double.infinity,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 18, vertical: 26),
                                  child: Row(
                                    textDirection: TextDirection.rtl,
                                    children: [
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            log("kdjfvb");
                                            Get.back(result: 200);
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.greenAccent,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            alignment: Alignment.center,
                                            child: Text("بله"),
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 8),
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            Get.back(result: 400);
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(
                                                  255, 253, 144, 144),
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "خیر",
                                              style: TextStyle(),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                        customer.highlightColor!.value = -1;
                        maincontroller.update();
                        if (res == 200) {
                          if (customer.waterMeterState!.value == 1) {
                            await SqliteService().changewaterMeterState(
                                id: customer.id!.string,
                                waterMeterState: 0,
                                note: "کنتور تعمیر شده است");
                            maincontroller.getCustomersFromDB();
                          } else {
                            await SqliteService().changewaterMeterState(
                                id: customer.id!.string,
                                waterMeterState: 1,
                                note: "کنتور خراب است");
                            maincontroller.getCustomersFromDB();
                          }
                        }
                      },
                      child: Container(
                        height: 35,
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        decoration: BoxDecoration(
                          color: Colors.orangeAccent,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          customer.waterMeterState!.value == 1
                              ? "کنتور تعمیر شده"
                              : "کنتور خراب است",
                          style: TextStyle(
                            fontSize: customer.waterMeterState!.value == 1
                                ? 12
                                : null,
                          ),
                        ),
                      ),
                    ),
                  ),
                  // SizedBox(width: 6),
                  Expanded(
                    flex: 8,
                    child: InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () async {
                        log("note btn");
                        customer.highlightColor!.value = 0;
                        maincontroller.counternewdatacontroller.text =
                            customer.note!.value;
                        maincontroller.update();
                        await Get.bottomSheet(
                          Container(
                            height: 250,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(22)),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(22)),
                                  child: appBar(customer.userCompany!.string),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 24),
                                  child: Text(
                                    "یادداشت",
                                    textAlign: TextAlign.right,
                                    textDirection: TextDirection.rtl,
                                    style: TextStyle(
                                      // fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black87,
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 43,
                                  // width: context.mediaQuerySize.width,
                                  margin: EdgeInsets.symmetric(horizontal: 18),
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 24.75),
                                  decoration: BoxDecoration(
                                    color: Color(0xffEDEDED),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: TextFormField(
                                    controller:
                                        maincontroller.counternewdatacontroller,
                                    onChanged: (e) {
                                      log("text: $e");
                                    },
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                      color: Color(0xff2F2C2C),
                                    ),
                                    maxLines: 1,
                                    minLines: 1,
                                    textAlign: TextAlign.left,
                                    textDirection: TextDirection.ltr,
                                    decoration: InputDecoration(
                                      hintTextDirection: TextDirection.rtl,
                                      alignLabelWithHint: true,
                                      hintStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w300,
                                        color: Color(0xff898D92),
                                      ),
                                      hintMaxLines: 1,
                                      border: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                        borderSide: BorderSide(
                                          color: Color(0xffFF3333),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 50,
                                  width: double.infinity,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 18, vertical: 10),
                                  child: Row(
                                    textDirection: TextDirection.rtl,
                                    children: [
                                      Expanded(
                                        child: InkWell(
                                          splashColor: Colors.transparent,
                                          highlightColor: Colors.transparent,
                                          onTap: () async {
                                            Position position = await Geolocator
                                                .getCurrentPosition(
                                              desiredAccuracy:
                                                  LocationAccuracy.high,
                                            );
                                            SqliteService().updateNote(
                                              lat: position.latitude,
                                              long: position.longitude,
                                                id: customer.id!.string,
                                                note: maincontroller
                                                    .counternewdatacontroller
                                                    .text);
                                            maincontroller.getCustomersFromDB();
                                            // 63501
                                            showToastMassage(
                                                msg: "تایید شد", success: true);
                                            maincontroller
                                                .counternewdatacontroller
                                                .clear();
                                            maincontroller.update();
                                            Get.back();
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.blue,
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "تایید",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w600,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 8),
                                      Expanded(
                                        child: InkWell(
                                          splashColor: Colors.transparent,
                                          highlightColor: Colors.transparent,
                                          onTap: () {
                                            Get.back();
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.blue),
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "بستن",
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                        maincontroller.counternewdatacontroller.text = "";
                        customer.highlightColor!.value = -1;
                        maincontroller.update();
                      },
                      child: Container(
                        height: 35,
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        margin: EdgeInsets.symmetric(horizontal: 6),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                            width: 0.5,
                            color: Colors.grey,
                          ),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          "یادداشت",
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 10,
                    child: InkWell(
                      onTap: () async {
                        customer.highlightColor!.value = 0;
                        maincontroller.update();
                        await Get.bottomSheet(
                          Container(
                            height: 250,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(22)),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(22)),
                                  child: appBar(customer.userCompany!.string),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 24),
                                  child: Text(
                                    "شماره کنتور فعلی",
                                    textAlign: TextAlign.right,
                                    textDirection: TextDirection.rtl,
                                    style: TextStyle(
                                      // fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black87,
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 43,
                                  // width: context.mediaQuerySize.width,
                                  margin: EdgeInsets.symmetric(horizontal: 18),
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 24.75),
                                  decoration: BoxDecoration(
                                    color: Color(0xffEDEDED),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: TextFormField(
                                    controller:
                                        maincontroller.counternewdatacontroller,
                                    onChanged: (e) {
                                      log("text: $e");
                                    },
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                      color: Color(0xff2F2C2C),
                                    ),
                                    maxLines: 1,
                                    minLines: 1,
                                    textAlign: TextAlign.left,
                                    textDirection: TextDirection.ltr,
                                    decoration: InputDecoration(
                                      hintTextDirection: TextDirection.rtl,
                                      alignLabelWithHint: true,
                                      hintStyle: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w300,
                                        color: Color(0xff898D92),
                                      ),
                                      hintMaxLines: 1,
                                      border: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                        borderSide: BorderSide(
                                          color: Color(0xffFF3333),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                // SizedBox(height: 18),
                                // Container(
                                //   margin: EdgeInsets.symmetric(horizontal: 24),
                                //   child: Text(
                                //     "شماره پیمایش",
                                //     textAlign: TextAlign.right,
                                //     textDirection: TextDirection.rtl,
                                //     style: TextStyle(
                                //       // fontSize: 18,
                                //       fontWeight: FontWeight.w600,
                                //       color: Colors.black87,
                                //     ),
                                //   ),
                                // ),
                                // Container(
                                //   height: 43,
                                //   // width: context.mediaQuerySize.width,
                                //   margin: EdgeInsets.symmetric(horizontal: 18),
                                //   padding:
                                //       EdgeInsets.symmetric(horizontal: 24.75),
                                //   decoration: BoxDecoration(
                                //     color: Color(0xffEDEDED),
                                //     borderRadius: BorderRadius.circular(10),
                                //   ),
                                //   child: TextFormField(
                                //     controller:
                                //         controller.recordingnumbercontroller,
                                //     // focusNode: node,
                                //     // onTap: onTap,
                                //     onChanged: (e) {
                                //       // controller.nameValidator(e);
                                //       log("text: $e");
                                //       // controller.update();
                                //     },
                                //     // onFieldSubmitted: (e) => controller.search(),
                                //     // obscureText: !showContent!,
                                //     style: TextStyle(
                                //       fontSize: 16,
                                //       fontWeight: FontWeight.w300,
                                //       color: Color(0xff2F2C2C),
                                //     ),
                                //     maxLines: 1,
                                //     minLines: 1,
                                //     textAlign: TextAlign.left,
                                //     textDirection: TextDirection.ltr,
                                //     decoration: InputDecoration(
                                //       hintTextDirection: TextDirection.rtl,
                                //       alignLabelWithHint: true,
                                //       hintStyle: TextStyle(
                                //         fontSize: 12,
                                //         fontWeight: FontWeight.w300,
                                //         color: Color(0xff898D92),
                                //       ),
                                //       hintMaxLines: 1,
                                //       border: InputBorder.none,
                                //       enabledBorder: InputBorder.none,
                                //       focusedBorder: InputBorder.none,
                                //       errorBorder: OutlineInputBorder(
                                //         borderRadius: BorderRadius.all(
                                //             Radius.circular(10)),
                                //         borderSide: BorderSide(
                                //           color: Color(0xffFF3333),
                                //           width: 1,
                                //         ),
                                //       ),
                                //     ),
                                //   ),
                                // ),
                                Spacer(),
                                Container(
                                  height: 50,
                                  width: double.infinity,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 18, vertical: 10),
                                  child: Row(
                                    textDirection: TextDirection.rtl,
                                    children: [
                                      Expanded(
                                        child: InkWell(
                                          splashColor: Colors.transparent,
                                          highlightColor: Colors.transparent,
                                          onTap: () async {
                                            if (double.parse(maincontroller
                                                    .counternewdatacontroller
                                                    .text) <
                                                customer.lastCheckoutNumber!
                                                    .value) {
                                              showToastMassage(
                                                msg:
                                                    "مقدار قرائت شده نباید کمتر از مقدار قبلی باشد !",
                                                success: false,
                                              );
                                              // Get.back();
                                            } else {
                                              Position position =
                                                  await Geolocator
                                                      .getCurrentPosition(
                                                desiredAccuracy:
                                                    LocationAccuracy.high,
                                              );
                                              int a = await SqliteService()
                                                  .recordCounter(
                                                id: customer.id!.string,
                                                ondate: DateTime.now()
                                                    .toIso8601String(),
                                                lastNumber: double.parse(
                                                    maincontroller
                                                        .counternewdatacontroller
                                                        .text),
                                                lat: position.latitude,
                                                long: position.longitude,
                                              );
                                              log(a.toString() + " :a");
                                              maincontroller
                                                  .getCustomersFromDB();
                                              log(customer.ondate!.string +
                                                  " onDate");
                                              // 63501
                                              showToastMassage(
                                                  msg: "تایید شد",
                                                  success: true);
                                              maincontroller
                                                  .counternewdatacontroller
                                                  .clear();
                                              customer.ondate!.value =
                                                  DateTime.now()
                                                      .toIso8601String();
                                              maincontroller.update();
                                              Get.back();
                                            }
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.blue,
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "تایید",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w600,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 8),
                                      Expanded(
                                        child: InkWell(
                                          splashColor: Colors.transparent,
                                          highlightColor: Colors.transparent,
                                          onTap: () {
                                            Get.back();
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.blue),
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "بستن",
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                // Spacer(),
                              ],
                            ),
                          ),
                        );
                        customer.highlightColor!.value = -1;
                        maincontroller.update();
                      },
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                        height: 35,
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        decoration: BoxDecoration(
                          color: Colors.greenAccent,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          "ثبت قرائت",
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    ),
  );
}
