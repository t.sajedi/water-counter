import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:water_counter/app/core/api/api.dart';
import 'package:water_counter/app/core/db/sqlite_service.dart';
import 'package:water_counter/app/models/customer.dart';
import 'package:water_counter/app/views/AppBar.dart';
import 'package:water_counter/app/views/Toast.dart';
import 'package:water_counter/app/views/customers.dart';
import 'package:water_counter/app/views/landing/widgets/gridElemnt.dart';
import 'package:water_counter/app/views/setting/setting_view.dart';
import '../../controller/landing_controller.dart';
import 'widgets/enterCodebtmSheet.dart';

class LandingView extends GetView<LandingController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LandingController>(
      builder: (controller) {
        return Scaffold(
          body: SafeArea(
            child: controller.isGettingData.isTrue
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Column(
                    children: [
                      InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () async {
                          // tmpList.forEach((element) {
                          //   log(element.id!.value.toString());
                          // });
                          // log(controller.mainController.customers.length.toString());
                          // Customer customer = controller.mainController.customers.first;
                          // log(customer.toMap()["isActive"].toString());
                          // SqliteService().createItem(customer);
                          // controller.mainController.customers.forEach((element) {
                          //   SqliteService().createItem(element);
                          // });
                        },
                        child: appBar("صفحه اصلی"),
                      ),
                      InkWell(
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Get.to(() => Customers())!.then((value) {
                            controller.initlens();
                          });
                        },
                        child: Card(
                          elevation: 6,
                          margin: EdgeInsets.symmetric(horizontal: 32),
                          child: Container(
                            height: 250,
                            width: context.width,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.my_library_books_outlined,
                                  size: 48,
                                  color: Colors.blue,
                                ),
                                SizedBox(height: 6),
                                Text(
                                  "لیست مشترکین",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                Text(
                                  "تعداد مشترکین بررسی شده : " +
                                      controller.recorderdCustomersLen
                                          .toString(),
                                  style: TextStyle(color: Colors.grey
                                      // fontWeight: FontWeight.w600,
                                      ),
                                ),
                                Text(
                                  "تعداد کل مشترکین : " +
                                      controller.customersLen.toString(),
                                  style: TextStyle(color: Colors.grey
                                      // fontWeight: FontWeight.w600,
                                      ),
                                ),
                                Text(
                                  "تعداد مشترکین ثبت نشده : " +
                                      controller.notRecordedLen.toString(),
                                  style: TextStyle(color: Colors.grey
                                      // fontWeight: FontWeight.w600,
                                      ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Expanded(
                        child: GridView.builder(
                          padding: EdgeInsets.symmetric(horizontal: 18),
                          itemCount: controller.gridList.length,
                          physics: BouncingScrollPhysics(),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 1.3,
                            crossAxisSpacing: 22,
                            mainAxisSpacing: 18,
                          ),
                          itemBuilder: (context, index) {
                            return InkWell(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onTap: () async {
                                switch (index) {
                                  case 0:
                                    log("0");
                                    log(controller.enterCode.value.toString());
                                    if (controller.mainController.url.isEmpty) {
                                      await Get.to(() => SettingView());
                                      if (controller
                                          .mainController.url.isNotEmpty) {
                                        await getDatabtmSheet();
                                        controller.initlens();
                                      }
                                    } else {
                                      await getDatabtmSheet();
                                      controller.initlens();
                                    }
                                    controller.update();
                                    break;
                                  case 1:
                                    log("1");

                                    controller.isGettingData.value = true;
                                    controller.update();
                                    List<Customer> sendingList =
                                        await SqliteService().getItems();
                                    log(sendingList.length.toString() +
                                        " list.len");
                                    List<Map<String, dynamic>>? body = [];
                                    Customer element = sendingList.first;
                                    log(element.lastUpdate!.value.toString() +
                                        " : element.lastUpdate!.value");
                                    // body.add({
                                    //   "code":
                                    //       controller.mainController.code.value,
                                    //   "id": sendingList.first.id!.value,
                                    //   "lastNumber":
                                    //       sendingList.first.lastNumber!.value,
                                    //   "waterMeterState": sendingList
                                    //       .first.waterMeterState!.value,
                                    //   "note": sendingList.first.note!.value,
                                    //   "indexOrder":
                                    //       sendingList.first.indexOrder!.value,
                                    //   "registerById": 1,
                                    //   "ondate": element.lastUpdate!.value == "0"
                                    //       ? null
                                    //       : element.lastUpdate!.value,
                                    //   "absence": element.absence!.value,
                                    //   "lat": element.lat!.value == -9999
                                    //       ? 0
                                    //       : element.lat!.value,
                                    //   "long": element.long!.value == -9999
                                    //       ? 0
                                    //       : element.long!.value,
                                    //   "lastUpdate":
                                    //       element.lastUpdate!.value == "0"
                                    //           ? null
                                    //           : element.lastUpdate!.value,
                                    // });
                                    sendingList.forEach((element) {
                                      // log(element.contractCode!.value
                                      //     .toString());
                                      body.add({
                                        "code": controller
                                            .mainController.code.value,
                                        "id": element.id!.value,
                                        "lastNumber": element.lastNumber!.value,
                                        "waterMeterState":
                                            element.waterMeterState!.value,
                                        "note": element.note!.value,
                                        "indexOrder": element.indexOrder!.value,
                                        "registerById": 1,
                                        "ondate":
                                            element.lastUpdate!.value == "0"
                                                ? null
                                                : element.lastUpdate!.value,
                                        "absence": element.absence!.value,
                                        "lat": element.lat!.value == -9999
                                            ? 0
                                            : element.lat!.value,
                                        "long": element.long!.value == -9999
                                            ? 0
                                            : element.long!.value,
                                        "lastUpdate":
                                            element.lastUpdate!.value == "0"
                                                ? null
                                                : element.lastUpdate!.value,
                                        // "lastUpdate": element.lastUpdate!.value,
                                      });
                                    });
                                    // log(body.toString());
                                    log(controller.mainController.url.value +
                                        "/api/ImportFile/AddWaterMeterReadListFromApp");
                                    request(
                                      APIForm(
                                        type: APIType.POST,
                                        host: controller
                                                .mainController.url.value +
                                            "/api/ImportFile/AddWaterMeterReadListFromApp",
                                        header: {
                                          'Content-Type': 'application/json'
                                        },
                                        body: body,
                                      ),
                                    ).then((value) {
                                      log(body.toString());
                                      log(value.statusCode.toString());
                                      log(value.body);
                                      controller.isGettingData.value = false;
                                      controller.update();
                                      if (value.statusCode == 200) {
                                        if (jsonDecode(
                                                value.body)["isSuccess"] ==
                                            true) {
                                          showToastMassage(
                                              msg: "عملیات با موفقیت انجام شد",
                                              success: true);
                                        } else {
                                          showToastMassage(
                                            msg: jsonDecode(
                                                    value.body)["messages"]
                                                .toString(),
                                            success: false,
                                          );
                                        }
                                      } else {
                                        showToastMassage(
                                            msg:
                                                "عملیات انجام نشد کد : ${value.statusCode}",
                                            success: false);
                                      }
                                    });

                                    break;
                                  case 2:
                                    log("2");
                                    Get.to(() => SettingView())!.then((value) {
                                      controller.initlens();
                                    });
                                    break;
                                  case 3:
                                    Get.bottomSheet(
                                      Container(
                                        height: 100,
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.vertical(
                                              top: Radius.circular(10)),
                                        ),
                                        alignment: Alignment.center,
                                        child: Text(
                                          "V 1.0.0",
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 18,
                                          ),
                                        ),
                                      ),
                                    );
                                    log("3");
                                    break;

                                  default:
                                }
                              },
                              child: gridElement(
                                icon: controller.gridList[index]["icon"],
                                title: controller.gridList[index]["title"],
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
          ),
        );
      },
    );
  }
}
