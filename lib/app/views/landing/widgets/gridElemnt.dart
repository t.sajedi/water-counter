import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget gridElement({
  required IconData icon,
  required String title,
}) {
  return Card(
    elevation: 4,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          size: 32,
          color: Colors.blue,
        ),
        SizedBox(
          height: 12,
        ),
        Text(
          title,
          style: TextStyle(
            color: Colors.grey,
          ),
        ),

      ],
    ),
  );
}
