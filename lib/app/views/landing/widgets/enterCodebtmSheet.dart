import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:water_counter/app/controller/landing_controller.dart';
import 'package:water_counter/app/core/api/api.dart';
import 'package:water_counter/app/core/db/sqlite_service.dart';
import 'package:water_counter/app/models/customer.dart';
import 'package:water_counter/app/views/Toast.dart';

getDatabtmSheet() async {
  await Get.bottomSheet(
    Builder(builder: (context) {
      return Container(
        height: 200,
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 22),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(36)),
        ),
        child: GetBuilder<LandingController>(
          initState: (state) {
            Future.delayed(Duration(microseconds: 1)).then((value) {
              state.controller!.enterCode.value =
                  state.controller!.mainController.customers.length == 0;
              state.controller!.update();
              log(state.controller!.enterCode.value.toString());
              log(state.controller!.mainController.customers.length.toString());
            });
          },
          builder: (controller) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  controller.enterCode.isTrue
                      ? "برای دریافت اطلاعات شناسه قرائت را وارد کنید"
                      : "شما دارای ${controller.customersLen.value} مشترک هستید, در صورت بارگذاری مجدد اطلاعات قبل از بین خواهند رفت, آیا برای این کار اطمینان دارید؟",
                  textAlign: TextAlign.right,
                  textDirection: TextDirection.rtl,
                ),
                SizedBox(height: 18),
                AnimatedContainer(
                  duration: Duration(milliseconds: 250),
                  height: controller.enterCode.isTrue ? 53 : 0,
                  width: controller.enterCode.isTrue ? context.width : 0,
                  padding: EdgeInsets.symmetric(horizontal: 24.75),
                  decoration: BoxDecoration(
                    color: Color(0xffEDEDED),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextFormField(
                    controller: controller.enterCode.isTrue
                        ? controller.codeController
                        : null,
                    // focusNode: node,
                    // onTap: onTap,
                    onChanged: (e) {
                      // controller.nameValidator(e);
                      log("text: $e");
                      controller.update();
                    },
                    // onFieldSubmitted: (e) => controller.search(),
                    // obscureText: !showContent!,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff2F2C2C),
                    ),
                    maxLines: 1,
                    minLines: 1,
                    textAlign: TextAlign.left,
                    textDirection: TextDirection.ltr,
                    decoration: InputDecoration(
                      hintTextDirection: TextDirection.rtl,
                      alignLabelWithHint: true,
                      hintStyle: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                        color: Color(0xff898D92),
                      ),
                      hintMaxLines: 1,
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(
                          color: Color(0xffFF3333),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
                Spacer(),
                Row(
                  textDirection: TextDirection.rtl,
                  children: [
                    SizedBox(width: 25),
                    InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () async {
                        log("0" + controller.enterCode.value.toString());
                        if (controller.enterCode.isFalse) {
                          log("1");
                          log((await SqliteService().deletItems()).toString() +
                              " : delet items");
                          controller.mainController.customers.value = [];
                          controller.initlens();
                          controller.enterCode.toggle();
                          controller.update();
                        } else {
                          controller.getData();
                        }
                      },
                      child: Text(
                        controller.enterCode.isTrue ? "دریافت" : "تایید",
                        textAlign: TextAlign.right,
                        style: TextStyle(),
                      ),
                    ),
                    SizedBox(width: 25),
                    InkWell(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        Get.back();
                      },
                      child: Text(
                        "انصراف",
                        textAlign: TextAlign.right,
                        style: TextStyle(),
                      ),
                    )
                  ],
                )
              ],
            );
          },
        ),
      );
    }),
  );
}
