import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controller/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      builder: (controller) {
        return Scaffold(
          body: Center(
            child: Text(
              'خوش آمدید',
              style: TextStyle(fontSize: 36.0),
            ),
          ),
        );
      },
    );
  }
}
