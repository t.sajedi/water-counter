import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget appBar(String title, {double margin = 18}) {
  return Builder(
    builder: (context) {
      return Container(
        height: 65,
        width: context.width,
        padding: EdgeInsets.only(right: 18),
        margin: EdgeInsets.only(bottom: margin),
        color: Colors.blue,
        alignment: Alignment.centerRight,
        child: Text(
          title,
          textAlign: TextAlign.right,
          textDirection: TextDirection.rtl,
          style: TextStyle(
            color: Colors.white,
            // fontSize: 20,
          ),
        ),
      );
    }
  );
}
