import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:water_counter/app/controller/main_controller.dart';
import 'package:water_counter/app/core/db/sqlite_service.dart';
import 'package:water_counter/app/views/AppBar.dart';
import 'package:water_counter/app/views/Toast.dart';

import '../../controller/setting_controller.dart';

class SettingView extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainController>(
      initState: (state) {
        Future.delayed(Duration(microseconds: 1)).then((value) {
          state.controller!.urlTextController.text = state.controller!.url.string;
        });
      },
      builder: (controller) {
        return Scaffold(
          body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                appBar("تنظیمات"),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text(
                    "آدرس سایت",
                    textAlign: TextAlign.right,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.black87,
                    ),
                  ),
                ),
                Container(
                  height: 53,
                  width: context.mediaQuerySize.width,
                  margin: EdgeInsets.symmetric(horizontal: 18),
                  padding: EdgeInsets.symmetric(horizontal: 24.75),
                  decoration: BoxDecoration(
                    color: Color(0xffEDEDED),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextFormField(
                    controller: controller.urlTextController,
                    // focusNode: node,
                    // onTap: onTap,
                    onChanged: (e) {
                      // controller.nameValidator(e);
                      log("text: $e");
                      controller.update();
                    },
                    // onFieldSubmitted: (e) => controller.search(),
                    // obscureText: !showContent!,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff2F2C2C),
                    ),
                    maxLines: 1,
                    minLines: 1,
                    textAlign: TextAlign.left,
                    textDirection: TextDirection.ltr,
                    decoration: InputDecoration(
                      hintTextDirection: TextDirection.rtl,
                      alignLabelWithHint: true,
                      hintStyle: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                        color: Color(0xff898D92),
                      ),
                      hintMaxLines: 1,
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(
                          color: Color(0xffFF3333),
                          width: 1,
                        ),
                      ),
                    ),
                  ),
                ),
                Spacer(),
                InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    controller.checkingUrl.isFalse
                        ? controller.setUrl(
                            urll: controller.urlTextController.text,
                            context: context)
                        : null;
                  },
                  child: Card(
                    color: Colors.lightBlue[200],
                    elevation: 4,
                    margin: EdgeInsets.all(18),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Container(
                      height: 65,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: controller.checkingUrl.isFalse
                          ? Text(
                              "تایید آدرس سایت",
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            )
                          : CircularProgressIndicator(),
                    ),
                  ),
                ),
                InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () async {
                    int res = await Get.bottomSheet(
                      Container(
                        height: 200,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(10)),
                        ),
                        child: Column(
                          children: [
                            Container(
                              margin:
                                  EdgeInsets.only(top: 18, right: 18, left: 18),
                              alignment: Alignment.centerRight,
                              child: Text(
                                "آیا مایل به حذف تمامی مشترکین خود هستید ؟",
                                textAlign: TextAlign.right,
                                textDirection: TextDirection.rtl,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black87,
                                ),
                              ),
                            ),
                            Spacer(),
                            Container(
                              height: 45,
                              width: double.infinity,
                              margin: EdgeInsets.symmetric(
                                  horizontal: 18, vertical: 26),
                              child: Row(
                                textDirection: TextDirection.rtl,
                                children: [
                                  Expanded(
                                    child: InkWell(
                                      onTap: () {
                                        Get.back(result: 200);
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.greenAccent,
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        alignment: Alignment.center,
                                        child: Text("بله"),
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Expanded(
                                    child: InkWell(
                                      onTap: () {
                                        Get.back(result: 400);
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Color.fromARGB(
                                              255, 253, 144, 144),
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        alignment: Alignment.center,
                                        child: Text(
                                          "خیر",
                                          style: TextStyle(),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                    if (res == 200) {
                      await SqliteService().deletItems();
                      controller.customers.value = [];
                      showToastMassage(
                          msg: "تمامی مشترکین ذخیره شده حذف شدند !",
                          success: true);
                    }
                  },
                  child: Card(
                    elevation: 4,
                    margin: EdgeInsets.symmetric(horizontal: 18),
                    color: Colors.red[200],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      height: 65,
                      width: double.infinity,
                      alignment: Alignment.center,
                      child: Text(
                        "حذف مشترکین",
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 28,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
