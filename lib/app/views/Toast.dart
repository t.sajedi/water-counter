import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

showToastMassage({required String msg, required bool success}) {
  Fluttertoast.showToast(
    msg: "  " + msg + "  ",
    backgroundColor:
        success ? Color.fromARGB(255, 45, 249, 150) : Colors.redAccent,
    textColor: success ? Colors.black : Colors.white,
    gravity: ToastGravity.BOTTOM,
  );
}
