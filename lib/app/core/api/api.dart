import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:get/get.dart';

Future<http.Response> request(APIForm form) async {
  http.Response res = http.Response("no Internet", 111);
  // HttpClientResponse response;
  // var headers = {
  //   'content-type': 'application/json',
  // };
  log("1 ");
  try {
    res = await core(form, form.header);
  } catch (e) {
    log(e.toString());
    // res = http.Response("no Internet", 111);
  }
  if (!(res.statusCode > 199 && res.statusCode < 499)) {
    log("${res.statusCode} body: ${res.body}");
    // http.Response temp =
    //     await Get.toNamed(Routes.NoNet, arguments: [res.statusCode, form])
    //         as http.Response;
    // return temp;
    return res;
  } else {
    
    return res;
  }

  // while (counter < 4) {
  //   await Get.toNamed(Routes.NoNet)!.then((value) {
  //     return value;
  //   });
  //   // res = await core(form, headers);
  // }
  // return res;

  // log("statusCode utils: " + res.statusCode.toString());

  // if (form.handleNet == true &&
  //     (199 > res.statusCode || res.statusCode > 499)) {
  //   log("Utils: hiiiiiiiiiiiiiiiiiiiiiiiii: ${form.path}");
  //   log("2 counter: $counter");
  //   Get.toNamed(Routes.NoNet, arguments: [res.statusCode, form])!.then((value) {
  //     log("3 counter: $counter");
  //     log("after");
  //     // await request(form);
  //     log("4 counter: $counter");
  //     res = value;
  //     return res;
  //   });
  // } else {
  //   log("retuned of Utils");
  //   log("5 counter: $counter");
  //   return res;
  // }
  // log("bug");
  // return res;
}

core(APIForm form, var headers) async {
  // MainController mainController = Get.find();
  // var authHeader = Pathes.authHeader(mainController.token.value);
  log("host: ${form.host}");
  switch (form.type) {
    case APIType.GET:
      {
        return await http.get(
          Uri.parse(form.host),
          headers: form.header,
        );
        // break;
        // request = http.MultipartRequest('GET', Uri.parse(form.host + form.path));
        // request.headers.addAll(headers);

        // response = (await request.send()) as HttpClientResponse;
        // return res;
      }
    case APIType.POST:
      {
        return await http.post(
          Uri.parse(form.host + form.path),
          body: jsonEncode(form.body),
          headers: form.header,
        );
        // break;
        // response = (await request.send()) as HttpClientResponse;
        // return res;
      }
    case APIType.PUT:
      {
        // response = (await request.send()) as HttpClientResponse;
        return await http.put(
          Uri.parse(form.host + form.path),
          body: jsonEncode(form.body),
          headers: form.header,
        );
        // return res;
        // break;
      }

    case APIType.DELETE:
      {
        // response = (await request.send()) as HttpClientResponse;
        return await http.delete(
          Uri.parse(form.host + form.path),
          headers: form.header,
        );
        // return res;
        // break;
      }
    // return res;
  }
}

class APIForm {
  dynamic body;
  var header;
  bool useHeader;
  APIType type;
  String host;
  String path;
  bool? handleNet;

  APIForm({
    required this.header,
    this.body,
    this.type = APIType.GET,
    this.host = '',
    this.path = '',
    this.handleNet = false,
    this.useHeader = false,
  });
}

enum APIType {
  POST,
  GET,
  PUT,
  DELETE,
}
