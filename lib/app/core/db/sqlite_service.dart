import 'dart:developer';

import 'package:geolocator/geolocator.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:water_counter/app/models/customer.dart';

class SqliteService {
  static final String databaseName = "database.db";
  static String customerTableName = "customers";
  static Database? db;

  static Future<Database?> initizateDb() async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, databaseName);
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      log("here");
      await createTable(db);
    });
    // log(db.toString());
    // if (db == null) {
    //   log('moz');
    //   return await openDatabase(path, version: 1,
    //       onCreate: (Database db, int version) async {
    //     log("here");
    //     await createTable(db);
    //   });
    // } else {
    //   log("not null");
    //   return db;
    // }
    // return db ??
    //     await openDatabase(path, version: 1,
    //         onCreate: (Database db, int version) async {
    //       await createTable(db);
    //     });
  }

  // "CREATE TABLE $customerTableName(id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT, waterMeterInstallType INTEGER, waterMeterState INTEGER, waterMeterType INTEGER, subscriptionNumber INTEGER, userCompanyId INTEGER, userCompany TEXT, contractCode INTEGER, companyAddress TEXT, pelak INTEGER, mobileNumber INTEGER ,faz INTEGER, counterBandNumber TEXT, tariffCode TEXT, waterMeterBranchDiameter TEXT, waterMeterBranchDiameterId INTEGER, siphonDiameter TEXT, lastCheckoutNumber INTEGER, waterConsumptionLimit REAL, note TEXT, currentNumber REAL, ratio REAL, currentWaterConsumption INTEGER, lastUpdateNumberOnDate TEXT, indexOrder INTEGER, isActive BOOLEAN)",
  static Future<void> createTable(Database database) async {
    log("000000000000000000");
    await database.execute(
        "CREATE TABLE if not exists $customerTableName (id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT, waterMeterInstallType INTEGER, waterMeterState INTEGER, waterMeterType INTEGER, subscriptionNumber INTEGER, userCompanyId INTEGER, userCompany TEXT, contractCode INTEGER, companyAddress TEXT, pelak TEXT, INTEGER , mobileNumber TEXT, faz TEXT, counterBandNumber TEXT, tariffCode TEXT, waterMeterBranchDiameter TEXT, waterMeterBranchDiameterId INTEGER, siphonDiameter TEXT, lastCheckoutNumber INTEGER, waterConsumptionLimit REAL, note TEXT, currentNumber REAL, ratio REAL, currentWaterConsumption INTEGER, lastUpdateNumberOnDate TEXT, indexOrder INTEGER, isActive BOOLEAN, ondate TEXT, lastNumber INTEGER, absence INTEGER, lat REAL, long REAL, lastUpdate TEXT)");
  }

  Future<int> createItem(Customer customer) async {
    final Database? db = await initizateDb();
    log(customer.toMap().toString());
    int id = 5;
    try {
      id = await db!.insert(customerTableName, customer.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
    } catch (e) {
      log("catch");
      log(e.toString());
    }
    return id;
  }

  Future<List<Customer>> getItems() async {
    final Database? db = await SqliteService.initizateDb();
    log(db!.isOpen.toString());
    final List<Map<String, Object?>> queryResult =
        await db.query(customerTableName);
    return queryResult.map((e) => Customer.fromMap(e)).toList();
  }

  Future<List<Customer>> getItemsWithQuery(String orderBy) async {
    final Database? db = await SqliteService.initizateDb();
    log(db!.isOpen.toString());
    final List<Map<String, Object?>> queryResult =
        await db.query(customerTableName, orderBy: orderBy);
    return queryResult.map((e) => Customer.fromMap(e)).toList();
  }

  Future<List> getrecordedItems() async {
    final Database? db = await SqliteService.initizateDb();
    final List<Map<String, Object?>> queryResult = await db!
        .rawQuery("SELECT * FROM $customerTableName WHERE lastUpdate != 0");
    return queryResult;
  }

  Future<List<Customer>> getnotrecordedItems() async {
    final Database? db = await SqliteService.initizateDb();
    log(db!.isOpen.toString());
    final List<Map<String, Object?>> queryResult = await db
        .rawQuery("SELECT * FROM $customerTableName WHERE lastNumber == 0");
    return queryResult.map((e) => Customer.fromMap(e)).toList();
  }

  Future recordCounter({
    required String id,
    required double lastNumber,
    required String ondate,
    required double lat,
    required double long,
  }) async {
    final Database? db = await SqliteService.initizateDb();
    log(db!.isOpen.toString());
    log(id);
    log(lastNumber.toString());
    log(ondate);
    log(lat.toString());
    log(long.toString());
    // );
    int res = await db.rawUpdate(
      "UPDATE $customerTableName SET ondate = ?, lat = ?, long = ?, lastNumber = ?, lastUpdate = ? WHERE id = ?",
      [ondate, lat, long, lastNumber, ondate, id],
    );
    log(res.toString());
    return res;
  }

  Future updateNote({
    required String id,
    required String note,
    required double lat,
    required double long,
  }) async {
    final Database? db = await SqliteService.initizateDb();
    log(db!.isOpen.toString());
    int res = await db.rawUpdate(
      "UPDATE $customerTableName SET note = ?, lat = ?, long  = ?, lastUpdate = ? WHERE id = ?",
      [note, lat, long, DateTime.now().toIso8601String(), id],
    );
    return res;
    // return queryResult.map((e) => Customer.fromMap(e)).toList();
  }

  Future changewaterMeterState(
      {required String id,
      required int waterMeterState,
      required String note}) async {
    Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    final Database? db = await SqliteService.initizateDb();
    log(db!.isOpen.toString());
    int res = await db.rawUpdate(
      "UPDATE $customerTableName SET  lat = ?, long  = ?, waterMeterState = ?, lastUpdate = ? WHERE id = ?",
      [
        position.latitude,
        position.longitude,
        waterMeterState,
        DateTime.now().toIso8601String(),
        id,
      ],
    );
    return res;
    // return queryResult.map((e) => Customer.fromMap(e)).toList();
  }

  Future changeabsenc({
    required String id,
    required int newAbsence,
  }) async {
    Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    log(newAbsence.toString() + " : 3 newabsenc");
    final Database? db = await SqliteService.initizateDb();
    int res = await db!.rawUpdate(
      "UPDATE $customerTableName SET  lat = ?, long  = ?,absence = ?,lastUpdate = ? WHERE id = ?",
      [
        position.latitude,
        position.longitude,
        newAbsence,
        DateTime.now().toIso8601String(),
        id
      ],
    );
    return res;
    // return queryResult.map((e) => Customer.fromMap(e)).toList();
  }

  Future<int> deletItems() async {
    final Database? db = await SqliteService.initizateDb();
    log(db!.isOpen.toString());
    return await db.delete(customerTableName);
  }

  Future<List<Customer>> search(String text) async {
    final Database? db = await SqliteService.initizateDb();
    log(db!.isOpen.toString());
    final List<Map<String, Object?>> queryResult = await db.query(
      customerTableName,
      where:
          // "lastUpdateNumberOnDate LIKE '%${text}%' OR lastCheckoutNumber LIKE '%${text}%' OR currentNumber LIKE '%${text}%' OR id LIKE '%${text}%' OR  waterMeterState LIKE '%${text}%' OR  subscriptionNumber LIKE '%${text}%' OR  userCompanyId LIKE '%${text}%' OR  contractCode LIKE '%${text}%' OR  companyAddress LIKE '%${text}%' OR  pelak LIKE '%${text}%' OR  mobileNumber LIKE '%${text}%'",
          "contractCode LIKE '%${text}%'",

    );
    List<Customer> tmp = queryResult.map((e) => Customer.fromMap(e)).toList();
    log(tmp.length.toString() + " : tmp.length");
    List<Customer> res = tmp
        .where((element) =>
            (element.contractCode!.value == int.parse(text)))
        .toList();
    log(res.length.toString() + " : res.length");

    return res;
  }
  // Future<List<Customer>> search(String text) async {
  //   final Database? db = await SqliteService.initizateDb();
  //   log(db!.isOpen.toString());
  //   final List<Map<String, Object?>> queryResult = await db.query(
  //     customerTableName,
  //     where:
  //         "userCompany = '%${text}%'",
  //   );
  //   log(queryResult.length.toString() + " : queryResult");
  //   return queryResult.map((e) => Customer.fromMap(e)).toList();
  // }
}
