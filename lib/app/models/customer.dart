// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';

class Customer {
  RxInt? id;
  RxInt? waterMeterInstallType;
  RxInt? waterMeterState;
  RxInt? waterMeterType;
  RxInt? subscriptionNumber;
  RxInt? userCompanyId;
  RxString? userCompany;
  RxInt? contractCode;
  RxString? companyAddress;
  RxString? pelak;
  RxString? mobileNumber;
  RxString? faz;
  RxString? counterBandNumber;
  RxString? tariffCode;
  RxString? waterMeterBranchDiameter;
  RxInt? waterMeterBranchDiameterId;
  RxString? siphonDiameter;
  RxInt? lastCheckoutNumber;
  RxDouble? waterConsumptionLimit;
  RxString? description;
  RxString? note;
  RxDouble? currentNumber;
  RxDouble? ratio;
  RxInt? currentWaterConsumption;
  RxString? lastUpdateNumberOnDate;
  RxInt? indexOrder;
  RxBool? isActive;
  RxBool? absence = false.obs;
  RxString? ondate;
  RxInt? lastNumber;
  RxInt? highlightColor = (-1).obs;
  RxDouble? lat;
  RxDouble? long;
  RxString? lastUpdate;
  Customer({
    this.id,
    this.waterMeterInstallType,
    this.waterMeterState,
    this.waterMeterType,
    this.subscriptionNumber,
    this.userCompanyId,
    this.userCompany,
    this.contractCode,
    this.companyAddress,
    this.pelak,
    this.mobileNumber,
    this.faz,
    this.counterBandNumber,
    this.tariffCode,
    this.waterMeterBranchDiameter,
    this.waterMeterBranchDiameterId,
    this.siphonDiameter,
    this.lastCheckoutNumber,
    this.waterConsumptionLimit,
    this.description,
    this.note,
    this.currentNumber,
    this.ratio,
    this.currentWaterConsumption,
    this.lastUpdateNumberOnDate,
    this.indexOrder,
    this.isActive,
    this.ondate,
    this.lastNumber,
    this.absence,
    this.highlightColor,
    this.lat,
    this.long,
    this.lastUpdate,
  });

  Customer copyWith({
    RxInt? id,
    RxInt? waterMeterInstallType,
    RxInt? waterMeterState,
    RxInt? waterMeterType,
    RxInt? subscriptionNumber,
    RxInt? userCompanyId,
    RxString? userCompany,
    RxInt? contractCode,
    RxString? companyAddress,
    RxInt? pelak,
    RxString? mobileNumber,
    RxString? faz,
    RxString? counterBandNumber,
    RxString? tariffCode,
    RxString? waterMeterBranchDiameter,
    RxInt? waterMeterBranchDiameterId,
    RxString? siphonDiameter,
    RxString? lastCheckoutNumber,
    RxDouble? waterConsumptionLimit,
    RxString? description,
    RxString? note,
    RxDouble? currentNumber,
    RxDouble? ratio,
    RxInt? currentWaterConsumption,
    RxString? lastUpdateNumberOnDate,
    RxInt? indexOrder,
    RxBool? isActive,
  }) {
    return Customer(
      id: id ?? this.id,
      waterMeterInstallType:
          waterMeterInstallType ?? this.waterMeterInstallType,
      waterMeterState: waterMeterState ?? this.waterMeterState,
      waterMeterType: waterMeterType ?? this.waterMeterType,
      subscriptionNumber: subscriptionNumber ?? this.subscriptionNumber,
      userCompanyId: userCompanyId ?? this.userCompanyId,
      userCompany: userCompany ?? this.userCompany,
      contractCode: contractCode ?? this.contractCode,
      companyAddress: companyAddress ?? this.companyAddress,
      mobileNumber: mobileNumber ?? this.mobileNumber,
      faz: faz ?? this.faz,
      counterBandNumber: counterBandNumber ?? this.counterBandNumber,
      tariffCode: tariffCode ?? this.tariffCode,
      waterMeterBranchDiameter:
          waterMeterBranchDiameter ?? this.waterMeterBranchDiameter,
      waterMeterBranchDiameterId:
          waterMeterBranchDiameterId ?? this.waterMeterBranchDiameterId,
      siphonDiameter: siphonDiameter ?? this.siphonDiameter,
      waterConsumptionLimit:
          waterConsumptionLimit ?? this.waterConsumptionLimit,
      description: description ?? this.description,
      note: note ?? this.note,
      currentNumber: currentNumber ?? this.currentNumber,
      ratio: ratio ?? this.ratio,
      currentWaterConsumption:
          currentWaterConsumption ?? this.currentWaterConsumption,
      lastUpdateNumberOnDate:
          lastUpdateNumberOnDate ?? this.lastUpdateNumberOnDate,
      indexOrder: indexOrder ?? this.indexOrder,
      isActive: isActive ?? this.isActive,
    );
  }

  Map<String, dynamic> toMap() {
    log(absence!.string);
    return <String, dynamic>{
      'id': id!.value,
      'waterMeterInstallType': waterMeterInstallType!.value,
      'waterMeterState': waterMeterState!.value,
      'waterMeterType': waterMeterType!.value,
      'subscriptionNumber': subscriptionNumber!.value,
      'userCompanyId': userCompanyId!.value,
      'userCompany': userCompany!.value,
      'contractCode': contractCode!.value,
      'companyAddress': companyAddress == null ? "" : companyAddress!.value,
      'pelak': pelak == null ? "" : pelak!.value,
      'mobileNumber': mobileNumber != null ? mobileNumber!.value : "0",
      'faz': faz == null ? "" : faz!.value,
      'counterBandNumber':
          counterBandNumber == null ? "" : counterBandNumber!.value,
      'tariffCode': tariffCode == null ? "" : tariffCode!.value,
      'waterMeterBranchDiameter': waterMeterBranchDiameter!.value,
      'waterMeterBranchDiameterId': waterMeterBranchDiameterId!.value,
      'siphonDiameter': siphonDiameter == null ? "" : siphonDiameter!.value,
      'lastCheckoutNumber': lastCheckoutNumber!.value,
      'waterConsumptionLimit': waterConsumptionLimit!.value,
      'description': description!.value,
      'note': note == null ? "" : note!.value,
      'currentNumber': currentNumber!.value,
      'ratio': ratio != null ? ratio!.value : 0.0,
      'currentWaterConsumption': currentWaterConsumption!.value,
      'lastUpdateNumberOnDate': lastUpdateNumberOnDate == null
          ? "0000-00-00T00:00:00"
          : lastUpdateNumberOnDate!.value,
      'indexOrder': indexOrder!.value,
      'isActive': isActive!.value,
      'ondate': ondate == null ? "0" : ondate!.value,
      'lastNumber': lastNumber == null ? 0 : lastNumber!.value,
      'absence': absence == null ? RxBool(false) : absence!.value,
      'lat': lat == null ? RxDouble(-9999) : lat!.value,
      'long': long == null ? RxDouble(-9999) : long!.value,
      "lastUpdate": lastUpdate == null ? "0" : lastUpdate!.value,
      // 'highlightColor': highlightColor== null ? (-1).obs : highlightColor!.value,
    };
  }

  factory Customer.fromMap(Map<String, dynamic> map) {
    return Customer(
      id: map['id'] != null ? RxInt(map['id']) : null,
      waterMeterInstallType: map['waterMeterInstallType'] != null
          ? RxInt(map['waterMeterInstallType'])
          : null,
      waterMeterState:
          map['waterMeterState'] != null ? RxInt(map['waterMeterState']) : null,
      waterMeterType:
          map['waterMeterType'] != null ? RxInt(map['waterMeterType']) : null,
      subscriptionNumber: map['subscriptionNumber'] != null
          ? RxInt(map['subscriptionNumber'])
          : null,
      userCompanyId:
          map['userCompanyId'] != null ? RxInt(map['userCompanyId']) : null,
      userCompany:
          map['userCompany'] != null ? RxString(map['userCompany']) : null,
      contractCode:
          map['contractCode'] != null ? RxInt(map['contractCode']) : null,
      companyAddress: map['companyAddress'] != null
          ? RxString(map['companyAddress'])
          : null,
      pelak: map['pelak'] != null ? RxString(map['pelak']) : null,
      mobileNumber:
          map['mobileNumber'] != null ? RxString(map['mobileNumber']) : null,
      faz: map['faz'] != null ? RxString(map['faz']) : null,
      counterBandNumber: map['counterBandNumber'] != null
          ? RxString(map['counterBandNumber'])
          : null,
      tariffCode:
          map['tariffCode'] != null ? RxString(map['tariffCode']) : null,
      waterMeterBranchDiameter: map['waterMeterBranchDiameter'] != null
          ? RxString(map['waterMeterBranchDiameter'])
          : null,
      waterMeterBranchDiameterId: map['waterMeterBranchDiameterId'] != null
          ? RxInt(map['waterMeterBranchDiameterId'])
          : null,
      siphonDiameter: map['siphonDiameter'] != null
          ? RxString(map['siphonDiameter'])
          : null,
      lastCheckoutNumber: map['lastCheckoutNumber'] != null
          ? RxInt(map['lastCheckoutNumber'])
          : null,
      waterConsumptionLimit: map['waterConsumptionLimit'] != null
          ? RxDouble(map['waterConsumptionLimit'])
          : null,
      description:
          map['description'] != null ? RxString(map['description']) : null,
      note: map['note'] != null ? RxString(map['note']) : null,
      currentNumber:
          map['currentNumber'] != null ? RxDouble(map['currentNumber']) : null,
      ratio: map['ratio'] != null ? RxDouble(map['ratio']) : null,
      currentWaterConsumption: map['currentWaterConsumption'] != null
          ? RxInt(map['currentWaterConsumption'])
          : null,
      lastUpdateNumberOnDate: map['lastUpdateNumberOnDate'] != null
          ? RxString(map['lastUpdateNumberOnDate'])
          : RxString("0000-00-00T00:00:00"),
      indexOrder: map['indexOrder'] != null ? RxInt(map['indexOrder']) : null,
      isActive: map['isActive'] != null ? RxBool(map['isActive'] == 1) : null,
      ondate: map['lastCheckoutNumberOnDate'] != null
          ? RxString(map['lastCheckoutNumberOnDate'])
          : "".obs,
      lastNumber: map['lastNumber'] != null ? RxInt(map['lastNumber']) : null,
      absence: RxBool(map["absence"] == 1),
      highlightColor: (-1).obs,
      lat: map["lat"] == null ? RxDouble(-9999) : RxDouble(map["lat"]),
      long: map["long"] == null ? RxDouble(-9999) : RxDouble(map["long"]),
      lastUpdate:
          map["lastUpdate"] != null ? RxString(map["lastUpdate"]) : "0".obs,
    );
  }

  String toJson() => json.encode(toMap());

  factory Customer.fromJson(String source) =>
      Customer.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Customer(id: $id, waterMeterInstallType: $waterMeterInstallType, waterMeterState: $waterMeterState, waterMeterType: $waterMeterType, subscriptionNumber: $subscriptionNumber, userCompanyId: $userCompanyId, userCompany: $userCompany, contractCode: $contractCode, companyAddress: $companyAddress, pelak: $pelak, mobileNumber: $mobileNumber, faz: $faz, counterBandNumber: $counterBandNumber, tariffCode: $tariffCode, waterMeterBranchDiameter: $waterMeterBranchDiameter, waterMeterBranchDiameterId: $waterMeterBranchDiameterId, siphonDiameter: $siphonDiameter, lastCheckoutNumber: $lastCheckoutNumber, waterConsumptionLimit: $waterConsumptionLimit, description: $description, note: $note, currentNumber: $currentNumber, ratio: $ratio, currentWaterConsumption: $currentWaterConsumption, lastUpdateNumberOnDate: $lastUpdateNumberOnDate, indexOrder: $indexOrder, isActive: $isActive, ondate: $ondate)';
  }

  @override
  bool operator ==(covariant Customer other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.waterMeterInstallType == waterMeterInstallType &&
        other.waterMeterState == waterMeterState &&
        other.waterMeterType == waterMeterType &&
        other.subscriptionNumber == subscriptionNumber &&
        other.userCompanyId == userCompanyId &&
        other.userCompany == userCompany &&
        other.contractCode == contractCode &&
        other.companyAddress == companyAddress &&
        other.pelak == pelak &&
        other.mobileNumber == mobileNumber &&
        other.faz == faz &&
        other.counterBandNumber == counterBandNumber &&
        other.tariffCode == tariffCode &&
        other.waterMeterBranchDiameter == waterMeterBranchDiameter &&
        other.waterMeterBranchDiameterId == waterMeterBranchDiameterId &&
        other.siphonDiameter == siphonDiameter &&
        other.lastCheckoutNumber == lastCheckoutNumber &&
        other.waterConsumptionLimit == waterConsumptionLimit &&
        other.description == description &&
        other.note == note &&
        other.currentNumber == currentNumber &&
        other.ratio == ratio &&
        other.currentWaterConsumption == currentWaterConsumption &&
        other.lastUpdateNumberOnDate == lastUpdateNumberOnDate &&
        other.indexOrder == indexOrder &&
        other.isActive == isActive;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        waterMeterInstallType.hashCode ^
        waterMeterState.hashCode ^
        waterMeterType.hashCode ^
        subscriptionNumber.hashCode ^
        userCompanyId.hashCode ^
        userCompany.hashCode ^
        contractCode.hashCode ^
        companyAddress.hashCode ^
        pelak.hashCode ^
        mobileNumber.hashCode ^
        faz.hashCode ^
        counterBandNumber.hashCode ^
        tariffCode.hashCode ^
        waterMeterBranchDiameter.hashCode ^
        waterMeterBranchDiameterId.hashCode ^
        siphonDiameter.hashCode ^
        lastCheckoutNumber.hashCode ^
        waterConsumptionLimit.hashCode ^
        description.hashCode ^
        note.hashCode ^
        currentNumber.hashCode ^
        ratio.hashCode ^
        currentWaterConsumption.hashCode ^
        lastUpdateNumberOnDate.hashCode ^
        indexOrder.hashCode ^
        isActive.hashCode;
  }
}
