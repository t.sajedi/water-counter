import 'dart:async';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:water_counter/app/controller/UpdatePage.dart';
import '../routes/app_pages.dart';

class SplashController extends GetxController {
  // final now = DateTime.now().obs;

  static const _storageKey = 'insatll';

  final time = 2.obs;

  @override
  void onInit() {
    log("on iniiiiiiiiiiiiiiiiiiiiiiiiiiiiiitt");
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    log("reaaaaaaaaaaaaaaaaaaaaasy");
    _startTimer();
  }

  @override
  void onClose() {
    super.onClose();
  }

  _startTimer() {
    Timer? _timer;
    _timer = Timer.periodic(
      Duration(seconds: 1),
      (timer) {
        if (time.value == 0) {
          _timer!.cancel();
          _redirect();
        } else {
          time.value--;
        }
      },
    );
  }

  _redirect() {
    log("now " + DateTime.now().toString());
    log("1 day ago" + DateTime(2022, 11, 23).toString());
    if (DateTime.now().isAfter(DateTime(2023, 1, 15))) {
      Get.offAndToNamed(Routes.Landing);
      // Get.to(() => UpdatePage());
    } else {
      Get.offAndToNamed(Routes.Landing);
    }
  }

  bool get _isFirstInstall {
    GetStorage? _storage = new GetStorage();

    var val = _storage.read(_storageKey);
    if (val == null) {
      _storage.write(_storageKey, false);
      return true;
    }
    return false;
  }
}
