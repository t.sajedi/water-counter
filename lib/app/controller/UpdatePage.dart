import 'package:flutter/material.dart';
import 'package:get/get.dart';


class UpdatePage extends GetView{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          "لطفا برنامه خود را بروزرسانی کنید" + "\n"+ "با تشکر از صبر و شکیبایی شما",
          textAlign: TextAlign.center,
          textDirection: TextDirection.rtl,
          style: TextStyle(
            fontSize: 18,
            color: Colors.grey,
          ),
        ),
      ),
    );
  }
}