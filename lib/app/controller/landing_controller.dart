import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:water_counter/app/controller/main_controller.dart';
import 'package:water_counter/app/core/api/api.dart';
import 'package:water_counter/app/core/db/sqlite_service.dart';
import 'package:water_counter/app/models/customer.dart';
import 'package:water_counter/app/views/Toast.dart';
import 'package:water_counter/app/views/setting/setting_view.dart';

class LandingController extends GetxController {
  RxBool isGettingData = false.obs;
  RxBool enterCode = false.obs;
  TextEditingController codeController = TextEditingController();
  MainController mainController = Get.find();
  RxInt customersLen = 0.obs;
  RxInt recorderdCustomersLen = 0.obs;
  RxInt notRecordedLen = 0.obs;

  List gridList = [];

  @override
  onInit() {
    enterCode.value = mainController.customers.isEmpty;
    log("enterCode: " + enterCode.value.toString());
    initlens();
    update();
    if (mainController.customers.isEmpty) {
      enterCode.value = true;
    }
    gridList = [
      {
        "icon": Icons.import_contacts_rounded,
        "title": "دریافت اطلاعات",
      },
      {
        "icon": Icons.send_to_mobile_rounded,
        "title": "ارسال اطلاعات",
        "ontap": () {
          log("1");
        },
      },
      {
        "icon": Icons.settings_applications_sharp,
        "title": "تنظیمات",
        "ontap": () {
          log("2");
          Get.to(SettingView());
        },
      },
      {
        "icon": Icons.warning_amber_rounded,
        "title": "درباره ما",
      }
    ];
    super.onInit();
  }


  initlens() async {
    // log(DateTime.now().toString() + " : getting all");
    List<Customer> all = await SqliteService().getItems();
    // log(DateTime.now().toString() + " : getting recorded");
    List recorded = await SqliteService().getrecordedItems();
    // log("getting notrecorded");
    // List<Customer> notrecorded = await SqliteService().getnotrecordedItems();
    // log("all: " + all.length.toString());
    // log(DateTime.now().toString() + " : rec");
    // log("notRec" + notrecorded.length.toString());
    customersLen.value = all.length;
    recorderdCustomersLen.value = recorded.length;
    notRecordedLen.value = customersLen.value - recorderdCustomersLen.value;
    // log(all[0].lastUpdate.toString());
    // log(recorded[0]["lastUpdate"].toString());
    // log(DateTime.now().toString() + ": beforUpdate");
    update();
    // log(DateTime.now().toString() + ": afterUpdate");
  }

  getData() async {
    Get.back();
    isGettingData.value = true;
    update();
    log("0");
    // mainController.getCustomers(codeController.text);
    await request(APIForm(
      type: APIType.GET,
      host: mainController.url.value +
          "/api/WaterChargeManagement/GetAllByReadCode?code=" +
          codeController.text,
      header: {
        'Content-Type': 'application/json',
      },
    )).then((value) async {
      log(value.statusCode.toString());
      if (value.statusCode == 200 && (jsonDecode(value.body)["isSuccess"])) {
        mainController.customers = ((jsonDecode(value.body)["data"]) as List)
            .map((e) => Customer.fromMap(e))
            .toList()
            .obs;
        log("customers len: " + mainController.customers.length.toString());
        customersLen.value = mainController.customers.length;
        for (var i = 0; i < mainController.customers.length; i++) {
          log(i.toString() + " : index of loop");
          await SqliteService()
              .createItem(mainController.customers.elementAt(i));
        }
        // mainController.customers.forEach((element) async {
        //   await SqliteService().createItem(element);
        // });
        mainController.setCode(codee: codeController.text);
      }
      showToastMassage(
          msg: jsonDecode(value.body)["messages"],
          success: jsonDecode(value.body)["isSuccess"]);
    });
    await initlens();
    isGettingData.value = false;
    update();
  }
}
