import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:water_counter/app/core/api/api.dart';
import 'package:water_counter/app/core/db/sqlite_service.dart';
import 'package:water_counter/app/core/helpers/api_helper.dart';
import 'package:water_counter/app/models/customer.dart';
import 'package:water_counter/app/views/Toast.dart';

class MainController extends GetxController {
  RxBool gettingData = false.obs;
  RxString changedCustomer = "".obs;
  RxString url = "".obs;
  String urlKey = "url";
  String codeKey = "code";
  RxString code = "".obs;
  TextEditingController urlTextController = TextEditingController();
  TextEditingController counternewdatacontroller = TextEditingController();
  TextEditingController recordingnumbercontroller = TextEditingController();
  TextEditingController searchcontroller = TextEditingController();
  RxBool isSearch = false.obs;
  RxString search = "".obs;
  RxBool checkingUrl = false.obs;

  RxBool selectingsort = false.obs;
  List sortingList = [
    "شماره اشتراک",
    "کد قرارداد",
    "شماره پلاک",
    "ثبت شده ها",
    "ثبت نشده ها",
  ];
  RxString sort = "".obs;

  RxList<Customer> customers = <Customer>[].obs;
  RxInt recordingIndex = (-1).obs;

  onInit() async {
    log("maincontrollerInit");
    url.value = getUrl();
    code.value = getCode();
    await getCustomersFromDB();
    urlTextController.text = url.value;
    update();
    super.onInit();
  }

  getCustomersFromDB() async {
    log(isSearch.value.toString() + " : isSearch");
    if (isSearch.isTrue) {
      customers.value = await SqliteService().search(searchcontroller.text);
      search.value = searchcontroller.text;
      update();
    } else {
      switch (sort.value) {
        case "شماره اشتراک":
          customers.value =
              await SqliteService().getItemsWithQuery("-subscriptionNumber");
          update();
          break;
        case "کد قرارداد":
          customers.value =
              await SqliteService().getItemsWithQuery("-contractCode");
          update();
          break;
        case "شماره پلاک":
          customers.value = await SqliteService().getItemsWithQuery("pelak");
          update();
          break;
        case "ثبت شده ها":
          customers.value = await SqliteService().getItemsWithQuery("-lastUpdate");
          update();
          break;

        case "ثبت نشده ها":
          customers.value = await SqliteService().getItemsWithQuery("+lastUpdate");
          update();
          break;
        default:
          log("here");
          customers.value =
              await SqliteService().getItemsWithQuery("+indexOrder");
          update();
      }
    }
    update();
  }

  String getUrl() {
    GetStorage? _storage = new GetStorage();

    var val = _storage.read(urlKey);
    if (val == null) {
      return "";
    }
    return val;
  }

  String getCode() {
    GetStorage? _storage = new GetStorage();

    var val = _storage.read(codeKey);
    if (val == null) {
      return "";
    }
    return val;
  }

  setUrl({required String urll, required BuildContext context}) async {
    if (urll[urll.length - 1] == "/") {
      log("hi");
      urll = urll.substring(0, urll.length - 1);
      log(urll);
    }
    checkingUrl.value = true;
    update();
    request(
      APIForm(
        host: urll + "/api/WaterChargeManagement/GetAllByReadCode?code=test",
        header: {'Content-Type': 'application/json'},
      ),
    ).then((value) async {
      log("mmd");
      log(value.statusCode.toString());
      log(value.body);
      checkingUrl.value = false;
      update();
      if (value.statusCode == 200) {
        GetStorage? _storage = new GetStorage();
        await _storage.write(urlKey, urll);
        url.value = urll;
        update();
        showToastMassage(msg: "آدرس سایت با موفقیت ثبت شد", success: true);
        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        //   backgroundColor: Colors.green[100],
        //   content: Text(
        //     "آدرس سایت با موفقیت ثبت شد",
        //     textDirection: TextDirection.rtl,
        //     textAlign: TextAlign.right,
        //     style: TextStyle(
        //       color: Colors.black,
        //     ),
        //   ),
        // ));
      } else {
        showToastMassage(msg: "آدرس سایت وارد شده اشتباه است", success: true);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: Colors.red[100],
          content: Text(
            "آدرس سایت وارد شده اشتباه است",
            textDirection: TextDirection.rtl,
            textAlign: TextAlign.right,
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ));
      }
    });
  }

  setCode({required String codee}) async {
    GetStorage? _storage = new GetStorage();
    await _storage.write(codeKey, codee);
    code.value = codee;
    update();
  }

  Future<bool> handleLocationPermission(BuildContext context) async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
              'Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }
}
