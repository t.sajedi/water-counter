import 'dart:async';

import 'package:get/get.dart';

class SettingController extends GetxController {
  final now = DateTime.now().add(Duration(days: 690)).obs;

  @override
  void onInit() {
    super.onInit();
    printInfo(info: 'onInit');
  }

  @override
  void onReady() {
    super.onReady();
    // printError(info: 'onReady');

    Timer.periodic(
      Duration(seconds: 1),
      (timer) {
        now.value = DateTime.now().add(Duration(days: 690));
        update();
      },
    );
  }

  @override
  void onClose() {
    super.onClose();
  }
}
