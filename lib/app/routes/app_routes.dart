part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const SPLASH = '/splash';
  static const Intro = '/intro';
  static const Landing = '/landing';
  static const HOME = '/home';
  static const SETTINGS = '/settings';
  static const MOZ = '/moz';

  // static const PRODUCTS = _Paths.HOME + _Paths.PRODUCTS;
  // ignore: non_constant_identifier_names
  // static String PRODUCT_DETAILS(String productId) => '$PRODUCTS/$productId';
}

// abstract class _Paths {
//   static const SPLASH = '/splash';
//   static const Landing = '/landing';
//   static const PRODUCTS = '/products';
//   static const PROFILE = '/profile';
//   static const SETTINGS = '/settings';
//   static const SIGNIN = '/signin';
//   static const PRODUCT_DETAILS = '/:productId';
// }
