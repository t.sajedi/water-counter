import 'package:get/get.dart';
// import 'package:irancell_academy/app/bindings/landing_binding.dart';
// import 'package:irancell_academy/app/views/landing/landing_view.dart';

import '../bindings/landing_binding.dart';
import '../bindings/splash_binding.dart';
import '../bindings/intro_binding.dart';

import '../views/splash/views/splash_view.dart';
import '../views/intro/views/intro_view.dart';
import '../views/landing/landing_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static final routes = [
    GetPage(
      name: Routes.SPLASH,
      page: () => SplashView(),
      binding: SplashBinding(),
      middlewares: [],
      title: null,
      children: [],
    ),
    GetPage(
      name: Routes.Intro,
      page: () => IntroView(),
      binding: IntroBinding(),
      middlewares: [],
      title: null,
      children: [],
    ),
    GetPage(
      name: Routes.Landing,
      page: () => LandingView(),
      bindings: [
        LandingBinding(),
      ],
      title: null,
      children: [],
    ),
  ];
}
